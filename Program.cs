﻿using System;

namespace lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            var planets = new Planets();
            while(true)
            {
                try
                {
                    Console.WriteLine("Menu:\n1. Read from file\n2. Read from console\n3. Edit planet\n4. Sort\n5. Delete planet\n6. Print all\n7. Exit");
                    var choice = int.Parse(Console.ReadLine());
                    switch (choice)
                    {
                        case 1:
                            Console.Write("Enter name of the file:");
                            var file = Console.ReadLine();
                            planets.ReadFromFile(file);
                            break;
                        case 2:
                            planets.ReadFromConsole();
                            break;
                        case 3:
                            Console.Write("Enter name of editable planet: ");
                            var planet = Console.ReadLine();
                            planets.Edit(planet);
                            break;
                        case 4:
                            Console.Write("Enter field name to sort:");
                            var field = Console.ReadLine();
                            planets.SortBy(field);
                            break;
                        case 5:
                            Console.Write("Enter name of removable planet:");
                            var name = Console.ReadLine();
                            planets.Remove(name);
                            break;
                        case 6:
                            planets.Print();
                            break;
                        case 7:
                            return;
                    }
                }
                catch
                {
                    continue;
                }
            }
            
        }
    }
}
