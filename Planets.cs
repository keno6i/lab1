﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.IO;

namespace lab1
{
    class Planets
    {
        public List<Planet> PlanetList { get; set; }

        public Planets()
        {
            PlanetList = new List<Planet>();
        }

        public void Add(Planet planet)
        {
            try
            {
                if (PlanetList.Where(x => x.Name == planet.Name).ToList().Count != 0)
                    throw new Exception($"The planet with name {planet.Name} already exists.");
                PlanetList.Add(planet);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void ReadFromFile(string file)
        {
            try
            {
                var path = Directory.GetCurrentDirectory() + $"/{file}";
                var list = File.ReadAllLines(path).ToList();
                foreach (var i in list)
                {
                    var values = i.Split("; ");
                    if (values.Length != 5 || PlanetList.Where(x => x.Name == values[0]).ToList().Count != 0)
                        continue;
                    var planet = Check(values);
                    if (planet != new Planet())
                        PlanetList.Add(planet);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        public void Print()
        {
            PlanetList.ForEach(x => Console.WriteLine(x));
        }

        public void SortBy(string field)
        {
            PlanetList = PlanetList.OrderBy(x => x.GetType().GetProperty(field).GetValue(x, null)).ToList();
        }

        public List<Planet> Search(string text)
        {
            return PlanetList.Where(x => x.ToString().Contains(text)).ToList();
        }

        public void Remove(string name)
        {
            var removable = PlanetList.Where(x => x.Name == name).FirstOrDefault();
            PlanetList.Remove(removable);
        }

        public void Edit(string name)
        {
            try
            {
                var current = PlanetList.Where(x => x.Name == name).FirstOrDefault();
                if(current == null)
                    throw new Exception("Not found");
                var editable = Read();
                if (editable == new Planet() || editable.Name == "")
                    throw new Exception("Error, cannot edit this planet");
                else
                {
                    if (current.Name != editable.Name)
                        throw new Exception("You cannot edit name of the planet.");
                    PlanetList[PlanetList.IndexOf(current)] = editable;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        
        public void ReadFromConsole()
        {
            var planet = Read();
            if (PlanetList.Where(x => x.Name == planet.Name).ToList().Count != 0)
                Console.WriteLine("Already exists in list");
            else if (!(planet == new Planet() || planet.Name == ""))
                PlanetList.Add(planet); 
        }

        private Planet Read()
        {
            Console.WriteLine("Enter name; diameter; distance from sun; weight; quantity of satellites:");
            var values = Console.ReadLine().Split("; ");
            return Check(values);
        }

        private Planet Check(string[] values)
        {
            try
            {

                if (values.Length != 5)
                    throw new Exception("Error, please recheck input data ");
                string name = values[0];
                if (!uint.TryParse(values[1], out uint diameter))
                    throw new Exception("Error, please recheck diameter field.");
                if (!uint.TryParse(values[2], out uint distanceFromSun))
                    throw new Exception("Error, please recheck distanceFromSun field.");
                if (!double.TryParse(values[3], out double weight) || weight < 0)
                    throw new Exception("Error, please recheck weight field.");
                if (!uint.TryParse(values[4], out uint satellites))
                    throw new Exception("Error, please recheck satellites field.");
                return new Planet(name, diameter, distanceFromSun, weight, satellites);
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new Planet();
            }
        }
    }
}
