﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab1
{
    class Planet
    {
        public string Name { get; set; }
        public uint Diameter { get; set; }
        public uint DistanceFromSun { get; set; }
        public double Weight { get; set; } 
        public uint Satellites { get; set; }

        public Planet()
        {
            Name = "";
            Diameter = 0;
            DistanceFromSun = 0;
            Weight = 0;
            Satellites = 0;
        }

        public Planet(string name, uint diameter, uint distanceFromSun, double weight, uint sattelites = 0)
        {
            Name = name;
            Diameter = diameter;
            DistanceFromSun = distanceFromSun;
            Weight = weight;
            Satellites = sattelites;
        }

        public override string ToString()
        {
            return $"{Name} {Diameter} {DistanceFromSun} {Weight} {Satellites}";
        }
    }
}
